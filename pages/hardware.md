---
layout: page
title: "Hardware"
description: "Details of Hardware I use"
permalink: /hardware/
---

# Laptop

I'm using a [Lenovo Thinkpad
E450](https://www.lenovo.com/in/en/laptops/thinkpad/thinkpad-e-series/ThinkPad-E450/p/22TP2TEE450),
Intel i5 model which I bought in June, 2016. [Debian](https://www.debian.org/)
is currently installed on it.

* Intel Core i5-5200U Processor
* 4 Gigabyte 1600MHz DDR3L Ram
* 1 Terabyte 5400 rpm Hard Disk Drive
* 14" 1366x768 HD Display
* Integrated Intel Graphics and dedicated Radeon R7 M260 Graphics

Since, the time I bought it, I've had following issues:

* Webcam drives not available for Linux so native webcam applications like
  Cheese do not work. This issue was **resolved** in newer kernels.
* On Centos 7.4 (1708 build), 3.10.0-693 series kernels giving Kernel Panic due
  to AMD gpu. This bug was already **resolved** in Linux kernel 4 series, though
  4.x kernels are not officially available on Centos 7 yet. Several workarounds
  are suggested in the RedHat discussion
  [thread](https://access.redhat.com/discussions/3166301) over the bug.

# Home Server

My home server is a [Dell PowerEdge
T30](https://www.dell.com/en-in/work/shop/cty/pdp/spd/poweredge-t30) Tower
Server which I bought in August, 2017. It's also running
[Debian](https://www.debian.org/).

* Intel Xeon E3-1225 v5 Processor
* 8 Gigabyte ECC DDR4 Ram
* 1 Terabyte 7200 rpm Hard Disk Drive
* Software Raid Support

I've done following upgrades:

* Samsung SSD 850 Evo 250GB

# Raspberry Pi(s)

### Raspberry Pi 3 B

* Quad-Core 1.2GHz 64bit CPU
* 1 Gigabyte Ram

### Raspberry Pi Zero W

* 1GHz, single-core CPU
* 512 Megabytes RAM
