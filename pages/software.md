---
layout: page
title: "Software"
description: "Softwares I use on daily basis"
permalink: "/software/"
---

I prefer using [Open
Source](https://en.wikipedia.org/wiki/Open-source_software) software over
proprietary software in most cases.  This page lists the software I use.


# Operating System(s)

I’m running [Debian](https://www.debian.org/) Stretch on both my
[Laptop](/hardware/#laptop) and [Home server](/hardware/#home-server). My
[Raspberry Pis](/hardware/#raspberry-pis) are running on Raspbian Stretch Lite
(Headless). I have a cloud server running Debian at
[Linode](https://www.linode.com/). I have a couple of Virtual Machines on my
Home server powered by [KVM](https://www.linux-kvm.org/), running Debian
Stretch and [Archlinux](https://www.archlinux.org/).

# Programs

I’m using [i3](https://i3-wm.org/) Window Manager standalone, which is a tiling
window manager for [X](https://x.org/) Window System which nicely utilizes the
screen. I spend most of my time in *rxvt-unicode* (urxvt) inside a
[tmux](https://tmux.github.io/) session. I use quite a lot of command line
based utilities like [moc](https://moc.daper.net/),
[ranger](https://ranger.github.io/), [pass](https://www.passwordstore.org/) et
cetera. I like [Vim](https://www.vim.org/) a lot and always have at least one
Vim instance open and do all text editing in Vim. I browse the web on the
[Chromium](https://www.chromium.org/) web browser.
[Thunderbird](https://www.thunderbird.net/) is my email client. I use a very
popular [Enigmail](https://www.enigmail.net/) extension to sign/encrypt/decrypt
my emails using GPG key.

I run a local DNS Server on [Raspberry Pi](/hardware/#raspberry-pi-3-b) powered
by [BIND](https://www.isc.org/downloads/bind/) 9 which combines Cloudflare’s
[Public DNS](https://developers.google.com/speed/public-dns/) with that of
[Opennic](https://www.opennic.org/) and resolves local domains. And as I run
multiple machines locally (Physical and Virtual) which runs web servers, I have
[Nginx](https://www.nginx.org/) Proxy setup on Raspberry Pi which forwards web
requests. Raspberry Pi also runs a
[ddclient](https://sourceforge.net/p/ddclient/wiki/Home/), which is a dynamic
DNS client. I run several [Docker](https://www.docker.com/) containers on my
[Home Server](/hardware/#home-server). I use
[Cgit](https://git.zx2c4.com/cgit/) on my person Git server.
