---
layout: page
title: "About this Site"
description: "Details about the website"
permalink: /site/
---

This website consists of 100% validated *HTML5* pages generated using
[Jekyll](https://jekyllrb.com) static site generator. Sitemap of the
website is available in *XML* format at [sitemap.xml](/sitemap.xml) and Atom
feed for blog posts is available at [feed.xml](/feed.xml).

The website uses [simple-template](https://st.argp.in/) for Jekyll, which I
maintain separately. Source code and website are hosted at
[Gitlab](https://gitlab.com/ankit-sites/ankit/). Static files are generated and
deployed using [Gitlab CI](https://gitlab.com/ankit-sites/ankit/pipelines/).
This website can also be accessed over *HTTPS*, thanks to
[Cloudflare](https://cloudflare.com/)'s [Universal
SSL](https://blog.cloudflare.com/introducing-universal-ssl/).

Following Jekyll plugins are being used:
* [jekyll-feed](https://github.com/jekyll/jekyll-feed)
* [jekyll-seo-tag](https://github.com/jekyll/jekyll-seo-tag)
* [jekyll-sitemap](https://github.com/jekyll/jekyll-sitemap)

The website is a proud member of [Viewable With Any
Browser](https://anybrowser.org/campaign/) Campaign.

# Bugs

As I mentioned above, I maintain the template, separately.  All the design and
template related bugs must be reported in the *simple-template* [issue
tracker](https://github.com/simple-template/jekyll/issues/). For problem in
content, broken link or anything else, create an issue
[here](https://gitlab.com/ankit-sites/ankit/issues/).
