---
layout: home
title: "Ankit R Gadiya"
permalink: "/"
---

# About Me

I’m Ankit R Gadiya. I'm from the North-Western part of India in the state of
Rajasthan. I currently live in Bengaluru. Check out my [family's
website](https://gadiya.org/) to know more.  I’m a GNU/Linux enthusiast and
admire [Free Software
Philosophy](https://en.wikipedia.org/wiki/Open-source_software). I write about
Free Software and my experience with them on my [weblog](/blog/).

I'm working as a Software Development Engineer in [Zauba
Cloud](https://zauba.cloud/). I’m also learning Computer Science and different
associated fascinating things by myself. The path that I’m roughly following is
one mentioned in [Teach Yourself Computer
Science](https://teachyourselfcs.com/). I like to do small and fun projects on
my own. Check out the [Projects](/projects/) page for more information.

To read about [software](/software/) or [hardware](/hardware/) that I use, you
can check out their respective pages. Check out the [site](/site/) page for
more information about this website.

GPG Key: [0x80BAFB16](/assets/pgp.txt)
