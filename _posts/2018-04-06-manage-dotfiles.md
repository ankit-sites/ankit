---
layout: post
title: "Manage Dotfiles"
date: 2018-04-10
description: "How to efficiently manage Dotfiles using commonly available tools"
---

# What are Dotfiles?

For those readers, who are not already familiar with the concept of Dotfiles,
Dotfiles literally means files with names starting with a ".", however, these
files are somewhat special under *UNIX* environment, as they are hidden. They
are used by a lot of softwares to customize the behaviour according to the
needs of user at the runtime. Therefore, these files can also be called
*Runtime Configuration* files. Generally, these files are found directly under
user's home directory, however, some softwares prefer to place them under
folders.  Now-a-days, a lot of softwares are using **$XDG_CONFIG_HOME**
directory, which by default is `~/.config` directory.  The files are mostly
regular text files which can be edited by any kind of text editor. Some
softwares though, prefer to generate these files using some sort of utility,
like `lxappearance` for GTK themes.

# Why is it important to manage Dotfiles?

Usually, under Linux environment, most of the customization you do on various
softwares, all stay under your home directory in the form of dotfiles. So, if
you want to move your settings/customization to other system or preserve them
while re-installing or changing the distributions, for the most part you just
have to keep the dotfiles in place and all your customizations will be
preserved. For example, you want to preserve the terminal colors in your
terminal or your email client configuration or your Bash prompt and aliases. All
these are stores in the form of dotfiles.

# Ways of managing Dotfiles

As I mentioned above, Dotfiles are scattered over different directories under
user's home directory and to manually manage each file individually is harder.
So, what we can do is place all the dotfiles under a folder and then create
symlinks, whereever the software expects the dotfiles. But again manually
creating and updating symlinks is mundane. Fortunately, GNU has a tool for this
purpose, Stow. Stow, according to GNU is a symlink farm manager, original
purpose of it is to keep track of various versions of softwares installed on
the system. However, for our purpose it is a perfect tool.

There are various other ways used by others to accomplish the same task, which
are nicely listed at [Dotfiles](https://dotfiles.github.io/) page which you
might want to check.

# Using GNU Stow

First, install GNU Stow using the package manager of your distribution. Most
popular distributions have it in there official repositories.

### Debian

```bash
$ apt-get install stow
```

### Archlinux

```bash
$ pacman -Syu stow
```

### CentOS

```bash
$ yum install stow
```

Then you need to create a directory which will act as a central place to store
all the actual dotfiles. Note that this directory needs to be a direct
sub-directory of home directory, otherwise you'll have to provide additional
options to GNU Stow.

```bash
$ mkdir ~/.dotfiles
```

Now, for each software you want to add dotfiles of, make a directory under
`~/.dotfiles`. So, let's say you want to add Vim's configuration files then
you need to create `~/.dotfiles/vim` directory.

```bash
$ mkdir ~/.dotfiles/vim
```

Now, move all the files concerned with Vim, under this directory keeping the
folder structure same as under home directory. So, `~/.vimrc` will go in
`~/.dotfiles/vim/.vimrc` and `~/.vim/<filename>` will go in
`~/.dotfiles/vim/.vim/<filename>` and so on.

Once all files are in the sub-directory of `~/.dotfiles` folder, to enable the
symlinks first go to the `.dotfiles` directory and run `stow` with program name,
in this case vim as argument.

```bash
$ cd ~/.dotfiles
$ stow vim
```

And Stow will automatically create all the symlinks for you. To verify go to the
home directory and run ls.

```bash
$ ls -l ~/.vimrc
lrwxrwxrwx 1 elliot elliot 20 Mar 15 20:00 .vimrc -> .dotfiles/vim/.vimrc
```

In case you want to remove the symlinks of the software, simply run the same
`stow` command with `-D` option and program name as arguments in the `.dotfiles`
directory.

```bash
$ cd ~/.dotfiles
$ stow -D vim
```

# Git integration

As all our actual files are under `.dotfiles` directory, we can use Git to
version control and transfer new changes across multiple machines. To do so,
first initiate Git repository.

```bash
$ cd ~/.dotfiles
$ git init
```

Next, add all the files and commit them.

```bash
$ git add .
$ git commit -m "Initial Commit"
```

Now, you can simply push it to a Git Hosting service such as Github and make
all your machines clone it. This will also enable you to push and fetch changes
immediately from any machine of yours.
